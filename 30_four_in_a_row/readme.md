In this classic tile-dropping board game for two players, you must try to get four of your tiles in a row horizontally, vertically, or diagonally, while preventing your opponent from doing the same. This program is similar to Connect Four.

The Program in Action
When you run fourinarow.py, the output will look like this:

Four in a Row, by Al Sweigart al@inventwithpython.com
--snip--
     1234567
    +-------+
    |.......|
    |.......|
    |.......|
    |.......|
    |.......|
    |.......|
    +-------+
Player X, enter a column or QUIT:
> 3

     1234567
    +-------+
    |.......|
    |.......|
    |.......|
    |.......|
    |.......|
    |..X....|
    +-------+
Player O, enter a column or QUIT:
> 5
--snip--
Player O, enter a column or QUIT:
> 4

     1234567
    +-------+
    |.......|
    |.......|
    |XXX.XO.|
    |OOOOXO.|
    |OOOXOX.|
    |OXXXOXX|
    +-------+
Player O has won!
