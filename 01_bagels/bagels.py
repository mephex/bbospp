import random

print("""
Bagels, a deductive logic game.\n
I am thinking of a 3-digit number. Try to guess what it is.
Here are some clues:
When I say:    That means:
  Pico         One digit is correct but in the wrong position.
  Fermi        One digit is correct and in the right position.
  Bagels       No digit is correct.
I have thought up a number.
 You have 10 guesses to get it.
""")
secret_number = random.randrange(100,999)
solved = 0
guess_counter = 0
guess = ""

def compare_numbers(guess, secret_number):
  guess_copy = str(guess)
  secret_number_copy = str(secret_number)
  if guess == secret_number:
    global solved 
    solved = 1
    return
  pico = 0
  fermi = 0
  for char in range(len(secret_number_copy)):
    if guess_copy[char] == secret_number_copy[char]:
      fermi += 1
    if guess_copy[char] in secret_number_copy:
      pico += 1
  
  if fermi:
    print("Fermi")
  elif pico:
    print("Pico")
  else:
    print("Bagels")

while not solved and guess_counter < 10:
  guess_counter += 1
  guess = input("Guess #" + str(guess_counter) + "\n> ")
  if len(guess) != 3:
    print("Invalid response, must be a 3 digit number!")
    continue  

  try:
    guess = int(guess)
  except ValueError:
    print("Invalid value, must be a 3 digit number!")
    continue

  if int(guess) < 100:
    print("Invalid response, must be a 3 digit number 100-999!")
    continue

  if solved == 0:
    compare_numbers(guess,secret_number)
if solved == 1:
  print("You got it! The secret number was " + str(secret_number) + "!!")
else:
  print("You lose! The secret number was " + str(secret_number) + " :(")



