This program is an animation of a deep cave that descends forever into the earth. Although short, this program takes advantage of the scrolling nature of the computer screen to produce an interesting and unending visualization, proof that it doesn’t take much code to produce something fun to watch. This program is similar to Project 58, “Rainbow.”

The Program in Action
When you run deepcave.py, the output will look like this:

Deep Cave, by Al Sweigart al@inventwithpython.com
Press Ctrl-C to stop.
####################          ########################################
####################         #########################################
####################          ########################################
####################          ########################################
#####################          #######################################
######################          ######################################
#####################          #######################################
####################          ########################################
###################          #########################################
--snip--
