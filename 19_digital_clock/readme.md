This program displays a digital clock with the current time. Rather than render numeric characters directly, the sevseg.py module from Project 64, “Seven-Segment Display Module,” generates the drawings for each digit. This program is similar to Project 14, “Countdown.”

When you run digitalclock.py, the output will look like this:

 __   __       __   __       __   __
|  | |__|  *   __|  __|  *   __| |__
|__|  __|  *   __|  __|  *   __| |__|

Press Ctrl-C to quit.
