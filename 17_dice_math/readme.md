This math quiz program rolls two to six dice whose sides you must add up as quickly as possible. But this program operates as more than just automated flash cards; it draws the faces of the dice to random places on the screen. The ASCII-art aspect adds a fun twist while you practice arithmetic.

The Program in Action
When you run dicemath.py, the output will look like this:

Dice Math, by Al Sweigart al@inventwithpython.com

Add up the sides of all the dice displayed on the screen. You have
30 seconds to answer as many as possible. You get 4 points for each
correct answer and lose 1 point for each incorrect answer.

Press Enter to begin...
                                     +-------+
                                     | O   O |
                                     |   O   |
                                     | O   O |
                                     +-------+


                                  +-------+
            +-------+             | O   O |  +-------+
            |     O |             |       |  | O     |
            |       |             | O   O |  |       |
            | O     |             +-------+  |     O |
            +-------+                        +-------+
Enter the sum: 13
--snip--
