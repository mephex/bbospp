This program mimics the “digital stream” visualization from the science fiction movie The Matrix. Random beads of binary “rain” stream up from the bottom of the screen, creating a cool, hacker-like visualization. (Unfortunately, due to the way text moves as the screen scrolls down, it’s not possible to make the streams fall downward without using a module such as bext.)

The Program in Action
When you run digitalstream.py, the output will look like this:

Digital Stream Screensaver, by Al Sweigart al@inventwithpython.com
Press Ctrl-C to quit.
                     0                      0
                     0                      0
   1            0    0    1               1 0                             1
   0            0    0    1         0     0 0        0                    0
   0            1    0    0         0     1 0 0      1               0    1
   0            1    0    0         1     011 1      1               0    1 0
   0            1    0    0         0     000 11     0               0  1 1 0
   1     1      0 1  0    1         1     110 10  1  0               1  0 1 0
         1    101 0       0         1     000 11  1  1               11 1 1 1
         0    100 1       0               11  00  0  1               01     0
      1  1    001 1       1               0   1  10  0               10     0
      0  0    010 0       1                   1  11  11              0      0
--snip--
